import Jumbotron from '../components/cards/Jumbotron'
import { Table, Button } from 'react-bootstrap';
import { Fragment, useEffect, useState } from 'react';
import Swal from 'sweetalert2'
import { useParams, useNavigate, useLocation } from 'react-router-dom';


export default function Cart() {
    const [name, setName] = useState('');
    const { id } = useParams();
    const [price, setPrice] = useState(0)
    const [quantity, setQuantity] = useState(0)
    const navigation = useNavigate()

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/products/buy/${id}`)
            .then(response => response.json())
            .then(data => {
                setName(data.name);
                setPrice(data.price);
            })
    }, [])

    const handleDecrement = () => {
        setQuantity(prevCount => prevCount - 1);
      };

    const handleIncrement = () => {
        setQuantity(prevCount => prevCount + 1);
      };

    const CheckOut = () => {
        Swal.fire({
            title: "Successfully CheckOut.",
            icon: 'success',
            text: 'Successfully CheckOut Product.'
        })
        navigation(-1)
      };

    return (
        <>
            <Jumbotron title="Shopping Cart" />

            <Table striped>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Sub-Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>{name}</td>
                        <td>{price}</td>
                        <td>{price*quantity}</td>
                        <td><Button className='btn btn-dark mx-3'  onClick={handleDecrement}>-</Button><span className='m-3'>
                            {quantity}
                            </span><Button className='btn btn-dark'  onClick={() => {handleIncrement()}}>+</Button></td>
                        <td>
                            <Button className='btn btn-danger ml-3'  onClick={() => { navigation(-1) }}>Remove</Button>
                        </td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td ></td>
                        <td><Button className='btn btn-success'  onClick={() => {CheckOut() }}>CheckOut</Button></td>
                        <td>Total</td>
                        <td>
                           Php {price*quantity}
                        </td>
                    </tr>

                </tbody>
            </Table>
        </>
    )
}