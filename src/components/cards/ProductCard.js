import { useState, useEffect, useContext } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';

import UserContext from '../../UserContext';
import { Link } from 'react-router-dom';

export default function ProductCard({ productsProp }) {
	console.log("PROP", productsProp)
	const { _id, name, description, isActive,price, stock } = productsProp;
	const [endUsers, setEndUsers] = useState(0);
	const [stocksAvailable, setStocksAvailable] = useState(0)

	const [isAvailable, setIsAvailable] = useState(true)

	const { user } = useContext(UserContext);

	useEffect(() => {
		if (stocksAvailable === 0) {
			setIsAvailable(false)
		}

	}, [stocksAvailable])


	function purchase() {

		if (stocksAvailable === 1) {
			alert("Congratulations!")
		}

		setEndUsers(endUsers + 1)
		setStocksAvailable(stocksAvailable - 1)

	}
	return (
		<Row className='mt-3'>
			{isActive?<Col xs={12} md={4} className='offset-md-4 offset-0'>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{
							description}</Card.Text>
						<Card.Subtitle>
							Price:</Card.Subtitle>
						<Card.Text>PhP {price}
						</Card.Text>

						<Card.Subtitle>
							Stocks Available:</Card.Subtitle>
						<Card.Text>{stock}
						</Card.Text>
						{
							(user.isAdmin) ?
								<Button as={Link} to={`/updateProduct/${_id}`} variant="primary" >Update product</Button>
								:
								<div className='d-flex justify-content-between'>
									<Button as={Link} to={user.id === null ? "/login" : `/products/${_id}`} variant="primary" cl >View Product</Button>
									
									<Button as={Link} to={user.id === null ? "/login" : `/cart/${_id}`} variant="info"  >Buy Now!</Button>
								</div>
								

						}

					</Card.Body>
				</Card>
			</Col>:""}
		</Row>
	)
}

