import { Row, Col } from 'react-bootstrap';


export default function Jumbotron({title, subtitle}){

	return(
		<div className='container-fluid bg-primary'>
			<Row>
				<Col className="text-center p-4 bg-light">
					<h1>{title}</h1>
						<p className='lead'>{subtitle}</p>
				</Col>
			</Row>
		</div>
		)
}