// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {

	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>DRUMSET</h2></Card.Title>
				        <Card.Text>
						Make your Drumming Voice heard with Pearl’s sound-sculpting array of premium percussion instruments; crafted to fit any playing personality, style, and budget.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		
			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>GUITARS</h2></Card.Title>
				        <Card.Text>
						Aria has been at the forefront of guitar and Japanese guitar building excellence for over 60 years, learn about their story and see how their ingenuity is exemplified in their guitar work!
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		
			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>KEYBOARDS/PIANO</h2></Card.Title>
				        <Card.Text>
						Pianos are intricate machines. Think about it, their many precise, moving parts work together to form the instrument we all know and love. Learning to play the piano is something I’d recommend to anyone, but the price barrier to begin might seem a little high
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>BASS GUITARS</h2></Card.Title>
				        <Card.Text>
						The two main categories of bass guitars are electric and acoustic. Electric basses tend to leave you with more options to choose from -- including solid-body and semi-hollow body basses. Let’s get to know more about each of these types of bass guitars.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>ACCESSORIES</h2></Card.Title>
				        <Card.Text>
						Behind any instrument or pro audio system, there is a complement of supporting accessories designed to help it perform. These may be used during a performance, recording or rehearsal, or they may be accessories meant for use between sessions.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			<Col xs={12} md={4} className="mb-3">
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>OTHER INSTRUMENTS</h2></Card.Title>
				        <Card.Text>
						When we talk about musical instruments, we often talk about them as being part of a family. That's because, just like in human families, the instruments in a particular family are related to each other. They are often made of the same types of materials, usually look similar to one another, and produce sound in comparable ways. Some are larger and some are smaller, just as parents are bigger than children.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		</Row>
		)
}